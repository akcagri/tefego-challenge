import 'package:tefego_challenge/data/entities/github_repository.dart';

abstract class GithubRepositoryRepo{
  Future<List<GithubRepository>> getRepos(int pageNumber);
}