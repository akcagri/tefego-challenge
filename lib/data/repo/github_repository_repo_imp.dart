import 'package:tefego_challenge/data/entities/github_repository.dart';
import 'package:tefego_challenge/data/service/github_repository_service.dart';
import 'package:tefego_challenge/domain/repos/github_repository_repo.dart';

class GithubRepositoryRepoImpl extends GithubRepositoryRepo{
  final GithubRepositoryService service;
  GithubRepositoryRepoImpl(this.service);


  @override
  Future<List<GithubRepository>> getRepos(int pageNumber) {
   return service.getRepos(pageNumber);
  }

}