import 'package:dio/dio.dart';
import 'package:tefego_challenge/data/entities/github_repository.dart';

class GithubRepositoryService {
  Dio dio = Dio();

  Future<List<GithubRepository>> getRepos(int pageNumber) async {
    var searchDate = DateTime.now().subtract(Duration(days: 30));

    var response = await dio.get(
        'https://api.github.com/search/repositories?q=created:>${searchDate.toString().substring(0,10)}&sort=stars&order=desc&per_page=15&page=$pageNumber');
    var reposList = response.data['items'] as List;
    return reposList.map((e) => GithubRepository.fromJson(e)).toList();
  }
}
