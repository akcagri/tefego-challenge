class Owner {
  Owner({
      String? login, 
      String? avatarUrl,}){
    _login = login;
    _avatarUrl = avatarUrl;
}

  Owner.fromJson(dynamic json) {
    _login = json['login'];
    _avatarUrl = json['avatar_url'];
  }
  String? _login;
  String? _avatarUrl;

  String? get login => _login;
  String? get avatarUrl => _avatarUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['login'] = _login;
    map['avatar_url'] = _avatarUrl;
    return map;
  }

}