import 'owner.dart';

class GithubRepository {
  GithubRepository({
      String? fullName, 
      String? description, 
      int? stargazersCount, 
      int? openIssues, 
      String? pushedAt, 
      Owner? owner,}){
    _fullName = fullName;
    _description = description;
    _stargazersCount = stargazersCount;
    _openIssues = openIssues;
    _pushedAt = pushedAt;
    _owner = owner;
}

  GithubRepository.fromJson(dynamic json) {
    _fullName = json['full_name'];
    _description = json['description'];
    _stargazersCount = json['stargazers_count'];
    _openIssues = json['open_issues'];
    _pushedAt = json['pushed_at'];
    _owner = json['owner'] != null ? Owner.fromJson(json['owner']) : null;
  }
  String? _fullName;
  String? _description;
  int? _stargazersCount;
  int? _openIssues;
  String? _pushedAt;
  Owner? _owner;

  String? get fullName => _fullName;
  String? get description => _description;
  int? get stargazersCount => _stargazersCount;
  int? get openIssues => _openIssues;
  String? get pushedAt => _pushedAt;
  Owner? get owner => _owner;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['full_name'] = _fullName;
    map['description'] = _description;
    map['stargazers_count'] = _stargazersCount;
    map['open_issues'] = _openIssues;
    map['pushed_at'] = _pushedAt;
    if (_owner != null) {
      map['owner'] = _owner?.toJson();
    }
    return map;
  }

}