import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tefego_challenge/presentation/cubit/repository_shower_cubit.dart';
import 'package:tefego_challenge/presentation/page/repository_shower.dart';

class RepositoryShowerApp extends StatelessWidget {
  const RepositoryShowerApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (context) => RepositoryShowerCubit()..getRepos(),
        child: RepositoryShower(),
      ),
    );
  }
}
