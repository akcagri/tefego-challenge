import 'package:flutter/material.dart';
import 'package:tefego_challenge/data/entities/github_repository.dart';

class RepositoryShowerListItem extends StatelessWidget {
  final GithubRepository item;

  const RepositoryShowerListItem({Key? key, required this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery
        .of(context)
        .size;
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: size.width * .25,
              width: size.width * .25,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(item.owner!.avatarUrl!))),
            ),
            SizedBox(
              width: size.width * .025,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.fullName!,
                    style: Theme
                        .of(context)
                        .textTheme
                        .subtitle1,
                  ),
                  Text(
                    item.description ?? '',
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyText2,
                  ),
                  SizedBox(
                    height: size.width * .025,
                  ),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            item.stargazersCount.toString(),
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText2,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: size.width * .025,
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '${item.openIssues.toString()}',
                            style: Theme
                                .of(context)
                                .textTheme
                                .bodyText2,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: size.width * .025,
                      ),
                      Expanded(
                          child: Text(
                              '${item.pushedAt!.substring(0, 10)} by ${item
                                  .owner!.login!}'))
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
