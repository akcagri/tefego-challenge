import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tefego_challenge/data/entities/github_repository.dart';
import 'package:tefego_challenge/presentation/cubit/repository_shower_cubit.dart';
import 'package:tefego_challenge/presentation/widget/repository_shower_list_item.dart';

class RepositoryShowerList extends StatefulWidget {
  final List<GithubRepository> repos;
  final bool hasReachedEnd;

  const RepositoryShowerList(
      {Key? key, required this.repos, this.hasReachedEnd = false})
      : super(key: key);

  @override
  _RepositoryShowerListState createState() => _RepositoryShowerListState();
}

class _RepositoryShowerListState extends State<RepositoryShowerList> {
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery
        .of(context)
        .size;

    return ListView.builder(
        controller: scrollController,
        itemCount: widget.repos.length + (widget.hasReachedEnd ? 1 : 0),
        itemBuilder: (c, i) {
          return i < widget.repos.length ? RepositoryShowerListItem(
            item: widget.repos[i],) : Center(
            child: CircularProgressIndicator(),);
        });
  }

  void _onScroll() {
    var cubit = BlocProvider.of<RepositoryShowerCubit>(context);
    if (scrollController.position.pixels >=
        scrollController.position.maxScrollExtent &&
        !widget.hasReachedEnd) {
      cubit.getNextPage();
    }
  }
}