import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tefego_challenge/presentation/cubit/repository_shower_cubit.dart';
import 'package:tefego_challenge/presentation/cubit/repository_shower_state.dart';
import 'package:tefego_challenge/presentation/widget/repository_shower_list.dart';

class RepositoryShower extends StatefulWidget {
  const RepositoryShower({Key? key}) : super(key: key);

  @override
  _RepositoryShowerState createState() => _RepositoryShowerState();
}

class _RepositoryShowerState extends State<RepositoryShower> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: BlocBuilder<RepositoryShowerCubit, RepositoryShowerState>(
          builder: (context, state) {
            if (state is RepositoryShowerLoading)
              return Center(child: CircularProgressIndicator());
            else if (state is RepositoryShowerLoaded)
              return Center(
                  child: RepositoryShowerList(
                    repos: state.repos,
                    hasReachedEnd: state.hasReachedEndOfResults,
                  ));
            else if (state is RepositoryShowerError)
              return Center(child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('${state.errorMessage}'),
              ));
            else
              return SizedBox();
          }),
    );
  }
}




