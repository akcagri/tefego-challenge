import 'package:tefego_challenge/data/entities/github_repository.dart';

abstract class RepositoryShowerState{
  const RepositoryShowerState();
}
class RepositoryShowerLoading extends RepositoryShowerState{
  const RepositoryShowerLoading();
}
class RepositoryShowerInitial extends RepositoryShowerState{
  const RepositoryShowerInitial();
}
class RepositoryShowerLoaded extends RepositoryShowerState{
  final List<GithubRepository> repos;
  final bool hasReachedEndOfResults;
  const RepositoryShowerLoaded(this.repos,{this.hasReachedEndOfResults = false});

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RepositoryShowerLoaded &&
          runtimeType == other.runtimeType &&
          repos == other.repos && hasReachedEndOfResults == other.hasReachedEndOfResults;

  @override
  int get hashCode => repos.hashCode;
}
class RepositoryShowerError extends RepositoryShowerState{
  final String errorMessage;
  const RepositoryShowerError(this.errorMessage);
}