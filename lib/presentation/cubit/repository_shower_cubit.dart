import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tefego_challenge/data/entities/github_repository.dart';
import 'package:tefego_challenge/data/repo/github_repository_repo_imp.dart';
import 'package:tefego_challenge/presentation/cubit/repository_shower_state.dart';
import 'package:tefego_challenge/data/service/github_repository_service.dart';

class RepositoryShowerCubit extends Cubit<RepositoryShowerState> {
  RepositoryShowerCubit() : super(RepositoryShowerInitial());

  Future<void> getRepos([List<GithubRepository> oldRepo = const []]) async {
    if (state is RepositoryShowerInitial) emit(RepositoryShowerLoading());
    try{
      var repos = await GithubRepositoryRepoImpl(GithubRepositoryService())
          .getRepos(oldRepo.length ~/ 15 + 1);
      emit(RepositoryShowerLoaded([...oldRepo, ...repos]));
    }
     on DioError catch(e){
      emit(RepositoryShowerError(e.message));
    }

  }

  void getNextPage() {
    var oldRepo = (state as RepositoryShowerLoaded).repos;
    emit(RepositoryShowerLoaded(oldRepo, hasReachedEndOfResults: true));
    getRepos(oldRepo);
  }
}
